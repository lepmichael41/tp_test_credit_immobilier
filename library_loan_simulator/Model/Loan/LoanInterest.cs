﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class LoanInterest
    {
        private double interestRate;
        public double InterestRate
        {
            get
            {
                return interestRate;
            }
            set
            {
                if (value >= 0.0)
                {
                    interestRate = value;
                }
                else
                {
                    throw new ArgumentException($"Invalid interest rate, value must be greater or equal to 0");
                }
            }
        }
    }
}
