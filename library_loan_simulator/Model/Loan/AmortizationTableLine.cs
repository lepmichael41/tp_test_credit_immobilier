﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_loan_simulator.Model.Loan
{
    public class AmortizationTableLine
    {
        public uint MonthNumber { get; private set; }
        public uint MonthlyPayment { get; private set; }
        public uint Interest { get; private set; }
        public uint LoanRefund { get; private set; }
        public uint AmountToPay { get; private set; }
        public AmortizationTableLine(uint monthNumber, uint monthlyPayment, uint interest, uint loanRefund, uint amountToPay)
        {
            MonthNumber = monthNumber;
            MonthlyPayment = monthlyPayment;
            Interest = interest;
            LoanRefund = loanRefund;
            AmountToPay = amountToPay;
        }
    }
}
